#! /bin/bash

EUNOMIA_BI_VERSION=${EUNOMIA_BI_VERSION:-dev-17032021r1-int}
EUNOMIA_SERVICES_NET=${EUNOMIA_SERVICES_NET:-eunomia-services-net}

# this script starts/stops Eunomia Ledger service

function printHelp() {
  echo "Usage: "
  echo "  run.sh <mode> [-c <channel name>] [-t <timeout>] [-d <delay>] [-f <docker-compose-file>] [-s <dbtype>] [-l <language>] [-o <consensus-type>] [-i <imagetag>] [-a] [-n] [-v]"
  echo "    <mode> - one of 'start', 'stop', 'restart'"
  echo "      - 'start' - bring up the network with docker-compose up"
  echo "      - 'stop' - clear the network with docker-compose down"
  echo "      - 'restart' - restart the network"
  echo "  run.sh -h (print this message)"
  echo
}


function initiate_repo(){
	git submodule update --init --recursive --jobs 3

	# checkout all submodules to the same tag
	#git submodule foreach 'git
}


function start_ledger_services(){
	#run first the network
	cd network
	./start-fabric.sh
	echo " Network started!!!"
	./test_chaincode_eunomiaTuple.sh

	cd ../
	# deploy blockchain-api
	echo "Deploying blockhain-api container"
	set -x
	FIRSTNET_ROOT_DIR=$(realpath ./network/first-network/) BLOCKCHAIN_API_ROOT_DIR=$(realpath ./blockchain-api/) TAG=${EUNOMIA_BI_VERSION} EUNOMIA_SERVICES_NET=${EUNOMIA_SERVICES_NET} \
	  docker-compose -f blockchain-api/docker-compose.yml pull
	FIRSTNET_ROOT_DIR=$(realpath ./network/first-network/) BLOCKCHAIN_API_ROOT_DIR=$(realpath ./blockchain-api/) TAG=${EUNOMIA_BI_VERSION} EUNOMIA_SERVICES_NET=${EUNOMIA_SERVICES_NET} \
	  docker-compose -f blockchain-api/docker-compose.yml up -d
	set +x
	# Done
	echo "Done!!!"

}

function stop_ledger_services(){
	echo "Stopping blockchain-api container."
	FIRSTNET_ROOT_DIR=$(realpath ./network/first-network/) BLOCKCHAIN_API_ROOT_DIR=$(realpath ./blockchain-api/) TAG=${EUNOMIA_BI_VERSION} EUNOMIA_SERVICES_NET=${EUNOMIA_SERVICES_NET} \
	  docker-compose -f blockchain-api/docker-compose.yml down

	echo "Stopping blockchain network"
	cd ./network/
	./stop-fabric.sh
	cd ../
	echo "Done!!!"

}

MODE=$1
shift
# Determine whether starting, stopping, restarting, generating or upgrading
if [ "$MODE" == "start" ]; then
  EXPMODE="Starting"
elif [ "$MODE" == "stop" ]; then
  EXPMODE="Stopping"
elif [ "$MODE" == "restart" ]; then
  EXPMODE="Restarting"
#elif [ "$MODE" == "generate" ]; then
#  EXPMODE="Generating certs and genesis block"
#elif [ "$MODE" == "upgrade" ]; then
#  EXPMODE="Upgrading the network"
else
  printHelp
  exit 1
fi

if [ "${MODE}" == "start" ]; then
  initiate_repo
  start_ledger_services
elif [ "${MODE}" == "stop" ]; then ## Clear the network
  stop_ledger_services
#elif [ "${MODE}" == "generate" ]; then ## Generate Artifacts
#  generateCerts
#  replacePrivateKey
#  generateChannelArtifacts
elif [ "${MODE}" == "restart" ]; then ## Restart the network
  stop_ledger_services
  start_ledger_services
#elif [ "${MODE}" == "upgrade" ]; then ## Upgrade the network from version 1.2.x to 1.3.x
#  upgradeNetwork
else
  printHelp
  exit 1
fi
