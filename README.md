# LEDGER SERVICE

## Starting all ledger services

* Create a docker network of the rest Eunomia services OR find it
```
docker network create eunomia-services-net
```
OR
```
export EUNOMIA_SERVICES_NET=eunomia-services-net
```

* Launch the network
*run.sh* will do the magic for you.
```
chmod +x ./run.sh
./run.sh start
```

## Stopping the ledger services

```
./run.sh stop
```


## Contributors
* Antonios Inglezakis ( inglezakis.a@unic.ac.cy) 
